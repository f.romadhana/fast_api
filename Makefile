.PHONY: clean
## clean : clean project files
clean:
	rm -rf build/ dist/ *.egg-info .eggs .coverage htmlcov .mypy_cache
	find . -name '*pycache' -exec rm -rf {} +

.PHONY: deps
## deps: install dependencies
deps:
	pip install -r requirements.txt

.PHONY: check
## check: Use mypy to lint source codes
check:
	mypy main.py

.PHONY: run
## run: Run main.py
run:
	uvicorn main:app --reload --log-level debug

.PHONY: build
## build: build a docker image for production
build:
	@if docker images | grep "fromadhana/fast_api"; then\
		docker rmi fromadhana/fast_api;\
	fi
	docker build -t fromadhana/fast_api -f prod.Dockerfile .

.PHONY: push
## push: push a docker image to registry
push:
	docker push fromadhana/fast_api

.PHONY: run
## docker-run: run entrypoint from docker image
docker-run:
	docker run --rm --name fast_api fromadhana/fast_api python3 main.py

.PHONY: help
all:help
#help: show this  help message
help: Makefile
	@echo
	@echo " Choose a command to run in "$(NAME)":"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' | sed -e 's/^/ /'
	@echo