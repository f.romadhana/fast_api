"""
    author: f.romadhana@gmail.com

"""

from fastapi import APIRouter #type: ignore
import pickle, json, pandas as pd #type: ignore

router: APIRouter = APIRouter()

def load_model(model_path):
    with open(model_path, 'rb') as f:
        return pickle.load(f)

@router.post("predictive-sentiment")
def predictive_sentiment(textdata:str):

    #read path clf
    path = "./api_forecast/model/clf/20220929130032018614_classifier.clf"

    #tfidf model
    path1 = "./api_forecast/model/vector/20220929130032018614_vectorizer.clf"
    
    #read NB
    model_clf = load_model(path)

    #read tfidf
    model_vector = load_model(path1)

    text = "{}".format(textdata)

    v_data = model_vector.transform([text]).toarray()

    y_preds = model_clf.predict(v_data)

    if y_preds == 1:

        details = {
                    'Result': 'Positive'
        }

        predict     = pd.DataFrame(details, index=['1'])
        
        out_var     = predict.to_json(orient='records')
        
        datajsonReturn = json.loads(out_var)

    else:
        details = {
                    'Result': 'Negative'

        }

        predict     = pd.DataFrame(details, index=['1'])

        out_var     = predict.to_json(orient='records')

        datajsonReturn = json.loads(out_var)

    return {"msg": datajsonReturn}


