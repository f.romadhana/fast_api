"""
    author: f.romadhana@gmail.com

"""

from fastapi import APIRouter #type: ignore
from api_forecast.controllers import get #type: ignore

api_router = APIRouter()

api_router.include_router(get.router, tags=["sentiment-analytics"])

