#library
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from prometheus_fastapi_instrumentator import Instrumentator #type: ignore

#from folder
from api_forecast.controllers import api_router


def create_app():

    app = FastAPI()

    app.include_router(api_router)

    app.add_middleware(
                CORSMiddleware,
                allow_origins=["*"],
                allow_credentials=True,
                allow_methods=["*"],
                allow_headers=["*"]
    )

    Instrumentator().instrument(app).expose(app)

    return app
